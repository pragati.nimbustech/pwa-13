import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';



@NgModule({
  declarations: [],
  imports: [MatTableModule, MatPaginatorModule, MatToolbarModule, CommonModule ],
  exports: [MatTableModule, MatPaginatorModule, MatToolbarModule],
})
export class MaterialModule { }
